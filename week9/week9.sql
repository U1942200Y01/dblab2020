create or replace view usa_customers as
select CustomerID, CustomerName, ContactName
from customers
where Country = "USA";

select *
from usa_customers join orders on usa_customers.CustomerID = orders.CustomerID;

create view products_below_avg_price as
select ProductID, ProductName, Price
from products
where Price < (select avg(Price) from products);

select * from products_below_avg_price;

