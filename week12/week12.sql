select * from proteins where accession = "Q9NQS3";
explain select * from proteins where accession = "Q9NQS3";

create index idx1 on proteins(accession);
select * from proteins where accession = "Q9NQS3";
explain select * from proteins where accession = "Q9NQS3";

alter table proteins drop index idx1; # to drop index

create index idx2 on proteins(pid);

explain select * from proteins where pid like "%HUMAN";